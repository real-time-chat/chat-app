# chat-app

Chat Application Front-end

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Screenshots of the running application

![](src/assets/Images/Screenshot1.png)

![](src/assets/Images/Screenshot2.png)

![](src/assets/Images/Screenshot3.png)
