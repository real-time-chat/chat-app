import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "@/components/Home";
import Room from "@/components/Room";
import store from "@/store";

Vue.use(VueRouter)

const routes=[
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/room',
        name: 'room',
        meta: { requiresAuth: true },
        component: Room
    }
    ]

const router = new VueRouter({
    mode: "history",
    routes
})

router.beforeEach((to, from, next) => {
    console.log(store.getters.isAuth)
    if (to.matched.some(record => record.meta.requiresAuth)) {
        console.log(store.getters.isAuth)
        store.dispatch('auth').then(()=>{
            console.log(!store.getters.isAuth)
            next()
        }).catch(err=>{
            console.log(err)
            next({
                path: '/',
                params: { nextUrl: to.fullPath }
            })
            console.log(store.getters.isAuth)
        })
    } else {
        next()
        console.log(store.getters.isAuth)
    }
})

export default router;
