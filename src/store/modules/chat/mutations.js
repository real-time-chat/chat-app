export const updateMessages = (state, data) => {
    let messages= state.messages
    if (Array.isArray(data)){
        state.messages=[...messages,...data]
    } else {
        messages.push(data)
        state.messages=messages
    }
}

export const updateUsers = (state,data) =>{
    state.users=data
}

export const clearMsg= state=>{
    state.messages=[]
}

export const onlineStatus = (state,data) =>{
    state.online=data
}

export const sendMsg = ()=>{
}
