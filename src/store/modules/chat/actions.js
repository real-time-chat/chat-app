import io from "socket.io-client"
const {socketURL} = require('../../../config/config')
const socket=io(socketURL);
import store from "@/store";

export const join2Room=({commit},data)=>{
    return new Promise(resolve=>{
        if(!store.getters.online){
            socket.emit('joinRoom', data)
            commit('onlineStatus',true)
            resolve(1)
        } else{
            resolve(1)
        }
    })
}

export const sendMessage=({commit},data)=>{
    return new Promise(resolve=>{
        socket.emit('chatMessage', data)
        commit('sendMsg')
        resolve(1)
    })
}

export const disconnect=({commit},data) =>{
    return new Promise(resolve=>{
        if(store.getters.online){
            socket.emit('disconnectU', data)
            commit('onlineStatus',false)
            commit('clearMsg')
            resolve(1)
        } else {
            resolve(1)
        }
    })
}

/*export const multiSessions=({commit},data)=>{
    return new Promise(resolve=>{
        commit('setMulti',data)
        resolve(1)
    })
}*/

// Get room and users
socket.on('roomUsers', users  => {
    store.dispatch('refreshUsers',users)
});

// Message from server
socket.on('message', messages => {
    store.dispatch('receiveMessages',messages)
});

/*socket.on('sessions',sessions=>{
    store.dispatch('multiSessions',sessions)
})*/

export const receiveMessages = ({commit},data)=>{
    return new Promise(resolve=>{
        commit('updateMessages',data)
        resolve(1)
    })
}

export const refreshUsers = ({commit},data)=>{
    return new Promise(resolve=>{
        commit('updateUsers',data)
        resolve(1)
    })
}
