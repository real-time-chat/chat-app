import axios from "axios"
const {apiURL} = require('../../../config/config')
import store from "@/store";

export const login = ({commit},data)=>{
    return new Promise((resolve, reject)=>{
        axios.post(apiURL + '/login', data).then(response=>{
            localStorage.setItem('access_token', response.data.token)
            localStorage.setItem('user_email',data.email)
            localStorage.setItem('user_name',response.data.user_name)
            localStorage.setItem('user_id',response.data.user_id)
            localStorage.setItem('moderator',response.data.moderator)
            commit('login',{
                access_token:response.data.token,
                user_email:data.email,
                user_name:response.data.user_name,
                user_id:response.data.user_id,
                moderator:response.data.moderator
            })
            resolve(1)
        }).catch((error) => {
            reject(error.response.data)
        })
    })
}

export const register = ({commit},data)=>{
    return new Promise((resolve, reject)=>{
        axios.post(apiURL + '/register', data).then(r=>{
            commit('register',r)
            resolve(1)
        }).catch((error) => {
            reject(error.response.data)
        })
    })
}

export const logout = ({commit})=>{
    return new Promise(resolve=>{
        if(store.getters.online){
            store.dispatch('disconnect',
                {id:store.getters.user_id, username:store.getters.user_name, room:'class'}).then(()=>{
                commit('logout')
                resolve(1)
            })
        } else{
            commit('logout')
            resolve(1)
        }
    })
}

export const auth= ({commit})=>{
    return new Promise((resolve, reject)=>{
        axios.get(apiURL + '/private',
            {headers:{Authorization: `Bearer ${localStorage.getItem('access_token')}`}})
            .then(response=>{
                commit('auth',response.data.message==="You have access")
                resolve(1)
        }).catch((error) => {
            reject(error.response.data)
        })
    })
}
