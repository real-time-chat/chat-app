export const logout = (state) => {
    state.access_token = '';
    state.user_email = '';
    state.user_name = '';
    state.user_id='';
    state.moderator='false';
    state.isAuth=false;
    localStorage.setItem('access_token','')
    localStorage.setItem('user_email','')
    localStorage.setItem('user_name','')
    localStorage.setItem('user_id','')
    localStorage.setItem('moderator','false')
}

export const login = (state, data) => {
    state.access_token = data.access_token;
    state.user_email= data.user_email;
    state.user_name=data.user_name;
    state.moderator=data.moderator?'true':'false';
    state.user_id=data.user_id
}

export const auth =(state,data)=>{
    state.isAuth=data
    if(!data){
        state.access_token = '';
        state.user_email = '';
        state.user_name = '';
        state.user_id='';
        state.moderator='false';
    }
}

export const register=(state,data)=>{
    console.log(data)
}
