export const user_email= state=>{
    return state.user_email;
}

export const user_name= state=>{
    return state.user_name;
}

export const isAuth = state => {
    return state.isAuth;
}

export const moderator = state => {
    return state.moderator==='true';
}

export const user_id= state =>{
    return state.user_id
}
