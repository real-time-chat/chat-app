export default {
    access_token: localStorage.getItem('access_token'),
    user_email:localStorage.getItem('user_email'),
    user_name:localStorage.getItem('user_name'),
    user_id:localStorage.getItem('user_id'),
    moderator:localStorage.getItem('moderator'),
    isAuth:false
}
