import Vue from 'vue'
import App from './App.vue'
import store from "./store";
import router from "./router";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import Vuelidate from "vuelidate/src";
import VueChatScroll from "vue-chat-scroll/dist/vue-chat-scroll";

Vue.config.productionTip = false

Vue.use(BootstrapVue);
Vue.use(IconsPlugin)
Vue.use(Vuelidate);
Vue.use(VueChatScroll)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
